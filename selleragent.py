import random


class SellerAgent:
    def __init__(self, max_seller_cost):
        self.cost = random.random() * max_seller_cost
        self.max_cost = max_seller_cost
        self.traded = False

    def form_ask_price(self):
        return self.cost + random.random() * (self.max_cost - self.cost)
