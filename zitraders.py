from buyeragent import BuyerAgent
from selleragent import SellerAgent
from data import Data
import random
from time import time
from multiprocessing import Pool


class ZITraders:
    def __init__(self):
        self.verbose_output = False
        number_of_buyers = 500000
        number_of_sellers = 500000
        max_buyer_value = 30.0
        max_seller_cost = 30.0
        self.max_number_of_trades = 50000000

        self.buyers = [BuyerAgent(max_buyer_value)
                       for _ in xrange(number_of_buyers)]

        self.sellers = [SellerAgent(max_seller_cost)
                        for _ in xrange(number_of_sellers)]

        self.transactions = Data()


    def do_trading(self):
        def cb(datuum):
            self.transactions.add_datuum(datuum)

        po = Pool()
        for i in xrange(self.max_number_of_trades):
            po.apply_async(match, (self.buyers, self.sellers), callback=cb)
        po.close()
        po.join()

        print("\nFinal stats: %i transactions at %2.2f average price; "
              "%2.2f standard deviation.\n"
              % (self.transactions.N,
                 self.transactions.get_average(),
                 self.transactions.get_std_dev()))

def match(buyers, sellers):
    buyer, seller = random.choice(buyers), random.choice(sellers)
    bid_price = buyer.form_bid_price()
    ask_price = seller.form_ask_price()
    if not buyer.traded and not seller.traded and bid_price > ask_price:
        transaction_price = (ask_price
                             + random.random()
                             * (bid_price - ask_price))
        buyer.traded = True
        buyer.price = transaction_price
        seller.traded = True
        seller.price = transaction_price

        return transaction_price
    else:
        return None

        #if self.verbose_output:
        #    print("Found two agents willing to trade: ")
        #    print("Price of %2.2f with buyer's bid of %2.2f "
        #          "and seller asking price of %2.2f"
        #          % (transaction_price, bid_price, ask_price))


if __name__ == '__main__':
    t = time()
    simulation = ZITraders()
    simulation.do_trading()
    print('time %2.2f' % (time() - t))

