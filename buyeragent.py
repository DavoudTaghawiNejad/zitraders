import random


class BuyerAgent:
    def __init__(self, max_buyer_value):
        self.value = random.random() * max_buyer_value
        self.traded = False

    def form_bid_price(self):
        return random.random() * self.value
